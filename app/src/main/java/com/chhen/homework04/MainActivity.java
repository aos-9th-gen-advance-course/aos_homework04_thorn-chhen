package com.chhen.homework04;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;

import com.google.firebase.auth.FirebaseAuth;

public class MainActivity extends AppCompatActivity {

    private Button mButtonLogout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mButtonLogout = findViewById(R.id.buttonLogout);

        // bind events
        mButtonLogout.setOnClickListener(v -> {
            onButtonLogoutClicked();
        });

    }

    private void onButtonLogoutClicked() {
        FirebaseAuth.getInstance().signOut();
        finish();
    }

}